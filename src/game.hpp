#ifndef GAME_HPP_INCLUDED
#define GAME_HPP_INCLUDED

#include <iostream>
#include <memory>

#include "scenes/mainMenu.hpp"
#include "scenes/partyConfigurator.hpp"
#include "scenes/party.hpp"
#include "constants.hpp"
#include "abstract/terminal.hpp"
#include "abstract/functions.hpp"
#include <pybind11/embed.h>
#include <pybind11/pybind11.h>

namespace py = pybind11;

class Game{

private:
    sf::RenderWindow window;

    std::shared_ptr<Scene> activeScene = 0;
    std::vector<Scene> sceneArray;

    //Scenes
    //MainMenu mainMenu;
    //PartyConfigurator partyConf;

    std::shared_ptr<MainMenu> mainMenu = 0;
    std::shared_ptr<PartyConfigurator> partyConf = 0;
    std::shared_ptr<Party> party = 0;

    std::vector<Scene> SceneArray;
    //functions
    void render();
    void handle_event(sf::Event event);
    void interpret();

    //Triggers
        //Draw
    bool drawMainMenu;
    bool drawPartyConf;
    bool drawParty;

public:
    Game();

    void run();

};

#endif // GAME_HPP_INCLUDED
