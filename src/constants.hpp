#ifndef DEF_CONSTANTS
#define DEF_CONSTANTS

#include <SFML/Graphics.hpp>

#define DEV_MODE

//#define FULLSCREEN_MODE



//Window
#define WIN_TITLE "Pablo"
#ifdef FULLSCREEN_MODE
    #define WIN_H static_cast<float>(sf::VideoMode::getDesktopMode().height)
    #define WIN_W static_cast<float>(sf::VideoMode::getDesktopMode().width)
	#define CARD_SCALE	0.4f
#else
    #define WIN_H 600
    #define WIN_W 1066
	#define CARD_SCALE	0.3f
#endif

#if defined(_WIN32) || defined(_WIN64) || defined(__CYGWIN__)
	#define PLATFORM_WIN
#endif
#if defined(__unix__) || defined(__linux__) || defined(__APPLE__) && defined(__MACH__)
	// linux, unix and apple systems should be recognized this way
	#define PLATFORM_POSIX
#endif







enum class State{
Recto,
Verso
};

enum class PackageType {
Package,
Cimetery
};

#endif // CONSTANTS_HPP_INCLUDED
