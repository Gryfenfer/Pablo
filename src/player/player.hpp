#ifndef PLAYER_HPP_INCLUDED
#define PLAYER_HPP_INCLUDED

#include "../objects/card.hpp"
#include "../../debug.hpp"
#include "../constants.hpp"
#include <iostream>
#include <memory>
#include <array>


class Player {
private:
	//std::shared_ptr<Card> hand[2][2];
	std::array<std::array<std::shared_ptr<Card>,2>, 2> hand;

protected:
	

public:

	Player();
	~Player();

	void switchCard(std::shared_ptr<Card>& card, unsigned int x, unsigned int y);
	void switchCard(std::shared_ptr<Card>& card, unsigned int i);
	std::shared_ptr<Card> getCard(unsigned int i) const;
	static sf::Vector2f nbToPos(unsigned int i);
	//std::shared_ptr<Card> getCard(unsigned int x,unsigned int y);
	virtual std::string choosePackage(const char*, const  char* color, int valeur);
	virtual int chooseCard();
};

#endif
