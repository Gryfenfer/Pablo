#include "player.hpp"

Player::Player() {
	hand[0].fill(NULL);
	hand[1].fill(NULL);
}

Player::~Player(){}

void Player::switchCard(std::shared_ptr<Card>& card, unsigned int x, unsigned int y) {
	//DebugLog(SH_SPE, "avant......................");
	this->hand[x][y].swap(card);
	//DebugLog(SH_SPE, "apres......................");
}

void Player::switchCard(std::shared_ptr<Card>& card, unsigned int i) {
	this->hand[Player::nbToPos(i).x][Player::nbToPos(i).y].swap(card);
}

std::shared_ptr<Card> Player::getCard(unsigned int i) const {
	switch (i) {
	case 0:
		return hand[0][0];
	case 1:
		return hand[1][0];
	case 2:
		return hand[0][1];
	case 3:
		return hand[1][1];
	}
}


sf::Vector2f Player::nbToPos(unsigned int i) {
	switch (i) {
	case 0:
		return sf::Vector2f(0, 0);
	case 1:
		return sf::Vector2f(1, 0);
	case 2:
		return sf::Vector2f(0, 1);
	case 3:
		return sf::Vector2f(1, 1);
	}
}

std::string Player::choosePackage(const char*, const  char* color, int valeur) { return NULL; };
int Player::chooseCard() { return NULL; }