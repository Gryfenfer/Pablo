#include "bot.hpp"


Bot::Bot() : Player::Player(){

	L = luaL_newstate();
	luaL_openlibs(L);
	luaL_loadfile(L, "script.lua");
	lua_pcall(L, 0, 0, 0);
	std::cout << "I:::::::::::::::::::I" << std::endl;
	std::cout << "0 : " << lua_tonumber(L, 0) << std::endl;
	std::cout << "1 : " << lua_tonumber(L, 1) << std::endl;
	std::cout << "2 : " << lua_tonumber(L, 2) << std::endl;
	std::cout << "last element : " << lua_tonumber(L, -1) << std::endl;
	lua_getglobal(L, "fact");
	std::cout << "II:::::::::::::::::::II" << std::endl;
	std::cout << "0 : " << lua_tonumber(L, 0) << std::endl;
	std::cout << "1 : " << lua_tonumber(L, 1) << std::endl;
	std::cout << "2 : " << lua_tonumber(L, 2) << std::endl;
	std::cout << "last element : " << lua_tonumber(L, -1) << std::endl;
	lua_pushnumber(L, 3);
	std::cout << "III:::::::::::::::::::III" << std::endl;
	std::cout << "0 : " << lua_tonumber(L, 0) << std::endl;
	std::cout << "1 : " << lua_tonumber(L, 1) << std::endl;
	std::cout << "2 : " << lua_tonumber(L, 2) << std::endl;
	std::cout << "3 : " << lua_tonumber(L, 3) << std::endl;
	std::cout << "last element : " << lua_tonumber(L, -1) << std::endl;
	lua_pcall(L, 1, 1, 0);
	std::cout << "IV:::::::::::::::::::IV" << std::endl;
	std::cout << "0 : " << lua_tonumber(L, 0) << std::endl;
	std::cout << "1 : " << lua_tonumber(L, 1) << std::endl;
	std::cout << "2 : " << lua_tonumber(L, 2) << std::endl;
	std::cout << "last element : " << lua_tonumber(L, -1) << std::endl;
	
	lua_settop(L, 0); //clear the stack
	//lua_pushnumber(L, 999);
	//lua_remove(L, lua_gettop(L)); //delete the first element of the stack
	std::cout << "V:::::::::::::::::::V" << std::endl;
	std::cout << "0 : " << lua_tonumber(L, 0) << std::endl;
	std::cout << "1 : " << lua_tonumber(L, 1) << std::endl;
	std::cout << "2 : " << lua_tonumber(L, 2) << std::endl;
	std::cout << "last element : " << lua_tonumber(L, lua_gettop(L)) << std::endl;
	
	
}

Bot::~Bot() {
	lua_close(L);
}

std::string Bot::choosePackage(const  char* number, const  char* color, int value) {
	
	lua_getglobal(L, "choosePackage");
	std::cout << "VI:::::::::::::::::::VI" << std::endl;
	std::cout << "0 : " << lua_tonumber(L, 0) << std::endl;
	std::cout << "1 : " << lua_tonumber(L, 1) << std::endl;
	std::cout << "2 : " << lua_tonumber(L, 2) << std::endl;
	std::cout << "3 : " << lua_tonumber(L, 3) << std::endl;
	std::cout << "4 : " << lua_tonumber(L, 4) << std::endl;
	std::cout << "last element : " << lua_tonumber(L, lua_gettop(L)) << std::endl;
	lua_pushstring(L, number);
	
	std::cout << "VII:::::::::::::::::::VII" << std::endl;
	std::cout << "0 : " << lua_tonumber(L, 0) << std::endl;
	std::cout << "1 : " << lua_tonumber(L, 1) << std::endl;
	std::cout << "2 : " << lua_tonumber(L, 2) << std::endl;
	std::cout << "3 : " << lua_tonumber(L, 3) << std::endl;
	std::cout << "4 : " << lua_tonumber(L, 4) << std::endl;
	std::cout << "last element : " << lua_tonumber(L, lua_gettop(L)) << std::endl;
	lua_pushstring(L, color);
	std::cout << "VIII:::::::::::::::::::VIII" << std::endl;
	std::cout << "0 : " << lua_tonumber(L, 0) << std::endl;
	std::cout << "1 : " << lua_tonumber(L, 1) << std::endl;
	std::cout << "2 : " << lua_tonumber(L, 2) << std::endl;
	std::cout << "3 : " << lua_tonumber(L, 3) << std::endl;
	std::cout << "4 : " << lua_tonumber(L, 4) << std::endl;
	std::cout << "last element : " << lua_tonumber(L, lua_gettop(L)) << std::endl;
	lua_pushnumber(L, value);
	std::cout << "IX:::::::::::::::::::IX" << std::endl;
	std::cout << "0 : " << lua_tonumber(L, 0) << std::endl;
	std::cout << "1 : " << lua_tonumber(L, 1) << std::endl;
	std::cout << "2 : " << lua_tonumber(L, 2) << std::endl;
	std::cout << "3 : " << lua_tonumber(L, 3) << std::endl;
	std::cout << "4 : " << lua_tonumber(L, 4) << std::endl;
	std::cout << "last element : " << lua_tonumber(L, lua_gettop(L)) << std::endl;
	lua_pcall(L, 3, 1, 0);
	/*args
	1 : num�ro de la carte
	2 : couleur de la carte
	3 : valeur de la carte
	*/

	std::string resultat = lua_tostring(L,lua_gettop(L));
	//std::string resultat = "cimetery";

	return resultat;

}



int Bot::chooseCard() {
	lua_getglobal(L, "chooseCard");
	lua_pcall(L, 0, 1, 0);
	int resultat = lua_tonumber(L, lua_gettop(L));
	return resultat;
}