#ifndef BOT_HPP_INCLUDED
#define BOT_HPP_INCLUDED

#include "../objects/package.hpp"
#include "player.hpp"
#include "../constants.hpp"
#include <boost/filesystem.hpp>
#include <fstream>
#include <iostream>
#include <tuple>
#include "../abstract/functions.hpp"
#include "lua.hpp"


//#include "../scripting/scripting.hpp"
//#include <boost/python.hpp>
//#include <Python.h>

class Bot : public Player{

public:
	Bot();
	~Bot();

	/*Package& choosePackage(Package& package, Package& cimetery);
	int switchPosition(); //si retourne -1 on envoie la carte au cimetiere*/
	virtual std::string choosePackage(const char* number, const  char* color, int value);
	virtual int chooseCard();

private:

	lua_State *L;

};

#endif

