#ifndef PACKAGE_HPP_INCLUDED
#define PACKAGE_HPP_INCLUDED

#include <SFML/Graphics.hpp>
#include "card.hpp"
#include <iostream>
#include <vector>
#include <fstream>
#include "../abstract/container.hpp"
#include "../json/json.h"
#include "../constants.hpp"
#include <random>

class Package : public sf::Drawable, public sf::Transformable{

private:
    std::vector<Card> m_cardArray;

	float m_width = 10;
	float m_height = 10;
	sf::FloatRect boundingBox;

public:
    Package(std::vector<Card>);
    Package();
    void shuffle();

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const; //Drawing function

	void setSize(float width, float height);
	void setSize(sf::Vector2f);

    void addCard(Card card);
    Card& getFirstCard();
    Card takeFirstCard();
    void clear();

	void setPosition(float x, float y);
	void setPosition(sf::Vector2f position);

	bool isEmpty() const;

	sf::FloatRect getLocalBounds() const;
	sf::FloatRect getGlobalBounds() const;


    Package &operator=(const Package&);

    static std::vector<Card> generatePackage(State state,Container<sf::Texture>& textures,std::ifstream& rule);
	bool hasClick(sf::Event event);
	
};

#endif // PACKAGE_HPP_INCLUDED
