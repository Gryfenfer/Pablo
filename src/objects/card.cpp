#include "card.hpp"



Card::Card(std::string color, std::string number,int value,sf::Texture& frontTexture,sf::Texture& backTexture){
    m_color = color;
    m_number = number;
    m_value = value;
    frontSprite.setTexture(frontTexture);
    backSprite.setTexture(backTexture);
}

Card::Card(){}

void Card::draw(sf::RenderTarget& target, sf::RenderStates states) const{
    states.transform *= getTransform();
    if(state == State::Recto){
        target.draw(frontSprite,states);
    }else{
        target.draw(backSprite,states);
    }
}

const sf::Sprite& Card::getSprite() const{

	if (state == State::Recto) {
		return frontSprite;

	}
	else { return backSprite; }

}

void Card::returnCard() {
	if (this->state == State::Recto) {
		state = State::Verso;
	}
	else { state = State::Recto; }
}

void Card::setState(State s) {
	state = s;
}

bool Card::hasClick(sf::Event event) {


	if (this->getGlobalBounds().contains(static_cast<float>(event.mouseButton.x), static_cast<float>(event.mouseButton.y))) {
		return true;
	}
	else {
		return false;
	}
}

void Card::prensentation() {
	DebugLog(SH_INFO, m_number + " de " + m_color);
}

std::string Card::getNumber() { return this->m_number; }
std::string Card::getColor(){ return this->m_color; }
int Card::getValue(){ return this->m_value; }

sf::FloatRect Card::getLocalBounds() const{ return sf::FloatRect(0.f, 0.f, this->getSprite().getLocalBounds().width, this->getSprite().getLocalBounds().height); }
sf::FloatRect Card::getGlobalBounds() const{ return getTransform().transformRect(getLocalBounds()); }