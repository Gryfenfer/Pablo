#ifndef CARD_HPP_INCLUDED
#define CARD_HPP_INCLUDED

#include <iostream>
#include <SFML/Graphics.hpp>
#include "../constants.hpp"
#include "../../debug.hpp"

class Card : public sf::Drawable, public sf::Transformable{
private:
    
	std::string m_color;
	std::string m_number;

    int m_value;
    State state;


    sf::Sprite frontSprite;
    sf::Sprite backSprite;



public:

    Card(std::string color, std::string number,int value,sf::Texture& frontTexture,sf::Texture& backTexture);
    Card();

	const sf::Sprite& getSprite() const;


	sf::FloatRect getLocalBounds() const;
	sf::FloatRect getGlobalBounds() const;

	void returnCard();
	void setState(State s);

	void prensentation();

    //virtual
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const; //Drawing function

	bool hasClick(sf::Event event);

	std::string getNumber();
	std::string getColor();
	int getValue();

};

#endif // CARD_HPP_INCLUDED
