#include "package.hpp"



Package::Package() {}

Package::Package(std::vector<Card> cardArray) {
	m_cardArray = cardArray;
}

void Package::shuffle() {
	std::mt19937 rand;
	rand.seed(10);

	int i = 0;
	Card temp;
	int nombreTire;
	int const taille = this->m_cardArray.size();

	for (i = 0; i < taille - 1; i++) {
		nombreTire = rand() % (taille - 1);
		temp = m_cardArray[i];
		m_cardArray[i] = m_cardArray[nombreTire];
		m_cardArray[nombreTire] = temp;

	}

	DebugLog(SH_INFO, "The package is shuffled");
}

void Package::draw(sf::RenderTarget& target, sf::RenderStates states) const {
	
	if (m_cardArray.size() > 0) {
		states.transform *= getTransform();

		target.draw(m_cardArray[0],states);
	}

}

std::vector<Card> Package::generatePackage(State state, Container<sf::Texture>& textures, std::ifstream& rule) {
	std::vector<Card> cardPackage;
	Json::Value root;
	
	rule >> root;

	std::string symbol;
	std::string color;
	for (unsigned int i = 0; i < root["colors"].size(); i++){
		color = root["colors"][i].asString();
		for (unsigned int j = 0; j < root["cards"][color]["symbols"].size(); j++) {
			symbol = root["cards"][color]["symbols"][j].asString();
			Card card = Card(color, symbol, root["cards"][color][symbol]["valeur"].asInt(), textures.get(root["cards"][color][symbol]["Texture"].asString()), textures.get("bkg-card"));
			card.setState(state);
			cardPackage.push_back(card);
			
		}
		
		//symbol.clear();
	}
	
	
	

	return cardPackage;
}

void Package::addCard(Card card) {
	Card c = card;
	c.setScale(1, 1);
	c.setPosition(0, 0);
	c.setRotation(0);
	this->m_cardArray.insert(m_cardArray.begin(),c);

}

Card& Package::getFirstCard() {
	return m_cardArray[0];

}

Card Package::takeFirstCard() {
Card card;
	card = m_cardArray[0];
	m_cardArray.erase(m_cardArray.begin());
	card.setScale(CARD_SCALE, CARD_SCALE);
	card.setPosition(this->getPosition());
	card.setRotation(this->getRotation());
	
	return card;
}

void Package::clear() {
	m_cardArray.clear();

}

Package& Package::operator=(const Package& pack) {
	m_cardArray = pack.m_cardArray;
	return *this;

}

bool Package::hasClick(sf::Event event) {

	
	if (this->getGlobalBounds().contains(static_cast<float>(event.mouseButton.x), static_cast<float>(event.mouseButton.y))) {
 		return true;
	}
	else {
		return false;
	}
}

void Package::setSize(float width, float height) { 
	this->boundingBox.width = width;
	this->boundingBox.height = height;

}

void Package::setSize(sf::Vector2f size){

	this->boundingBox.width = size.x;
	this->boundingBox.height = size.y;
}

void Package::setPosition(float x, float y) {
	Transformable::setPosition(x, y);
	boundingBox.left = x;
	boundingBox.top = y;
}

void Package::setPosition(sf::Vector2f position) {
	Transformable::setPosition(position.x, position.y);
	boundingBox.left = position.x;
	boundingBox.top = position.y;
}

sf::FloatRect Package::getLocalBounds() const {
	return sf::FloatRect(0.f, 0.f, this->m_cardArray[0].getSprite().getLocalBounds().width, this->m_cardArray[0].getSprite().getLocalBounds().height);

}

sf::FloatRect Package::getGlobalBounds() const {
	return getTransform().transformRect(getLocalBounds());

}

bool Package::isEmpty() const {
	if (m_cardArray.size() == 0) {
		return true;
	}
	else { return false; }
}