#include "button.hpp"

Button::Button(){
    enable = true;
    hasText = false;

    if (!font.loadFromFile("assets/fonts/arial.ttf"))
    {
        //std::cout << "error" << std::endl;
    }
    text.setFont(font);
    textColor = sf::Color::Red;
    text.setFillColor(textColor);
    this->setPosition(0,0);
    //this->setScale(1,1);


}



void Button::draw(sf::RenderTarget& target,sf::RenderStates states) const{
	

	states.transform *= getTransform();
	
    target.draw(bkg,states);
    if (hasText){target.draw(text);}
	
}

void Button::setText(std::string txt){
    text.setString(txt);
    text.setOrigin(text.getGlobalBounds().width/2,text.getGlobalBounds().height/2);
    text.setPosition(this->getPosition().x + bkg.getGlobalBounds().width/2, this->getPosition().y + bkg.getGlobalBounds().height/2);
    hasText = true;
}

void Button::setTexture(sf::Texture& texture){
    this->bkg.setTexture(texture);
    //bkg.setScale(this->getScale());
    //bkg.setPosition(this->getPosition());
}

void Button::setEnable(bool changeOpacity){
    if(enable){enable = false;}else{enable = true;}
    if(changeOpacity && !enable){
        this->bkg.setColor(sf::Color(255,255,255,128));
        this->text.setFillColor(sf::Color(textColor.r,textColor.g,textColor.b,128));
    }
    else if(changeOpacity && enable){
        this->bkg.setColor(sf::Color(255,255,255,255));
        this->text.setFillColor(textColor);
    }
}

void Button::setScale(float factorX, float factorY) {
	Transformable::setScale(factorX, factorY);

}


bool Button::hasClick(sf::Event event){
	
   //if(bkg.getGlobalBounds().contains(event.mouseButton.x,event.mouseButton.y)){
	if(sf::FloatRect(this->getPosition().x, this->getPosition().y,bkg.getLocalBounds().width * this->getScale().x, bkg.getLocalBounds().height * this->getScale().y).contains(event.mouseButton.x, event.mouseButton.y)){
        return true;
    }
    else{
        return false;
    }
}

bool Button::getEnable(){
    return enable;
}

sf::Sprite Button::getBkg(){
    return bkg;
}
