#ifndef TYPE_HPP_INCLUDED
#define TYPE_HPP_INCLUDED

#include <string>
#include <vector>
#include <SFML/Graphics.hpp>

namespace Color{


    std::vector<sf::string> loadColors(){
        std::vector<std::string> colors;
        colors.push_back("Coeur");
        colors.push_back("Carreau");
        colors.push_back("Pique");
        colors.push_back("Trefle");
        return colors;
    }

}

#endif // TYPE_HPP_INCLUDED
