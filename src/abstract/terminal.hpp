#ifndef TERMINAL_HPP_INCLUDED
#define TERMINAL_HPP_INCLUDED

#include <iostream>
#include <string>
#include <vector>
#include <sstream>
#include <stdio.h>
#include <string.h>

std::vector<std::string> split_command(std::string cmd);

void interpret(std::string cmd);

void openShell();



#endif // TERMINAL_HPP_INCLUDED
