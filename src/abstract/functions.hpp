#ifndef FUNCTIONS_HPP_INCLUDED
#define FUNCTIONS_HPP_INCLUDED

#include <iostream>
#include <vector>

namespace func {

	std::vector<std::string> split(std::string chaine, char caractere);
}

#endif
