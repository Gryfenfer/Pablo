#include "functions.hpp"

std::vector<std::string> func::split(std::string chaine, char caractere) {
	unsigned int m = 0; //On utilise un marqueur m est le point de d�part pour d�couper la chaine
	std::vector<std::string> splitedString; //Tableau avec la chaine de caractere d�coup�e
	for (unsigned int i = 0; i < chaine.size(); i++) {
		if (chaine[i] == caractere) { // Si on reconnait le caractere
			splitedString.push_back(chaine.substr(m, i - m)); //On lui ajoute ce qu'il y avait avant (depuis le marqueur)
			m = i + 1; //update du marqueur
		}

	}
	splitedString.push_back(chaine.substr(m, chaine.size() - m)); //On lui ajoute la fin de la chaine de caractere
	return splitedString; 
}