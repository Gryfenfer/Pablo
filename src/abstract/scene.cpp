#include "scene.hpp"

Scene::Scene(int sizeX, int sizeY, sf::Vector2f position, sf::Color backgroundColor, double opacity){
    m_sizeX = sizeX;
    m_sizeY = sizeY;
    m_position = position;
    m_backgroundColor = backgroundColor;
    m_opacity = opacity * 255;

    //Make the background
    backgroundRectangle = sf::RectangleShape(sf::Vector2f(m_sizeX,m_sizeY));
    m_backgroundColor.a = opacity * 255;
    backgroundRectangle.setFillColor(m_backgroundColor);


    //MainView of the scene
    mainView.reset(sf::FloatRect(m_position.x,m_position.y,m_sizeX,m_sizeY));
    mainView.setViewport(sf::FloatRect(0, 0, 1.f, 1.f));

}

Scene::~Scene(){
    //Destroy all elements of the scene
}

void Scene::draw(sf::RenderTarget& target, sf::RenderStates states) const {
}

//Setters
void Scene::setOpacity(double arg){
    m_backgroundColor.a = arg * 255;
    backgroundRectangle.setFillColor(m_backgroundColor);
}
void Scene::setBackgroundColor(sf::Color color){
    m_backgroundColor = color;
}

std::string Scene::getButtonClickAction(){
    return this->buttonClickAction;
}
void Scene::resetButtonClickAction(){
    this->buttonClickAction = "";
}

//Getters
int Scene::getHeight() const {return m_sizeY;}
int Scene::getWidth() const {return m_sizeX;}
sf::Vector2f Scene::getPosition() const {return m_position;}
sf::View Scene::getMainView() const {return mainView;}
