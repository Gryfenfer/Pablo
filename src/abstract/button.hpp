#ifndef BUTTON_HPP_INCLUDED
#define BUTTON_HPP_INCLUDED

#include <SFML/Graphics.hpp>
#include "../../debug.hpp"

class Button : public sf::Drawable, public sf::Transformable{
private:
    sf::Sprite bkg;
    sf::Text text;
    bool enable;
    sf::Color textColor;

    bool hasText;

    sf::Font font;

public:
    Button();
    void setText(std::string txt);
    void setTexture(sf::Texture& texture);
    void setEnable(bool changeOpacity = true);

	void setScale(float factorX, float factorY);

    virtual void draw(sf::RenderTarget& target,sf::RenderStates states) const;

    bool hasClick(sf::Event);
    bool getEnable();
    sf::Sprite getBkg();
};


#endif // BUTTON_HPP_INCLUDED
