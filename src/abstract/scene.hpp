#ifndef SCENE_HPP_INCLUDED
#define SCENE_HPP_INCLUDED

#include <SFML/Graphics.hpp>
#include "../../debug.hpp"


class Scene : public sf::Transformable, public sf::Drawable{

public:
    //Constructors
    Scene(int sizeX = 15,int sizeY = 15,sf::Vector2f position = sf::Vector2f(0.f,0.f),sf::Color backgroundColor = sf::Color::Black, double opacity = 1);
    Scene();
    //Destructor
    virtual ~Scene();

    virtual void handle_std_animation() = 0;
    virtual void handle_std_event(sf::Event event) = 0;

    //Setters
    void setOpacity(double arg);
    void setBackgroundColor(sf::Color color);

    //Getters functions
    int getHeight() const;
    int getWidth() const;
    sf::Vector2f getPosition() const;
    sf::View getMainView() const;



protected:
    //virtual
    virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const = 0; //Drawing function


    sf::RectangleShape backgroundRectangle; //Background (we use a shape for it)
    sf::Color m_backgroundColor; //Background color of the scene, color of the rectangleShape
    double m_opacity; //Background's opacity
    float m_sizeX; //Scene's width
    float m_sizeY; //Scene's height
    sf::Vector2f m_position; //Scene's position
    sf::View mainView;

    std::string buttonClickAction = "";

public:

    std::string getButtonClickAction();
    void resetButtonClickAction();


};

#endif // SCENE_HPP_INCLUDED
