#include "party.hpp"

Party::Party(unsigned int nbPlayer,std::string rule) : Scene::Scene(WIN_W,WIN_H,sf::Vector2f(0.f,0.f),sf::Color(255,255,255),1){

	



	if (!font.loadFromFile("assets/fonts/arial.ttf"))
	{
		std::cout << "error" << std::endl;
	}
	

    sf::Texture cardTexture;
    cardTexture.loadFromFile("assets/cards/13-pique.png");
    sf::Texture cardTextureBkg;
    cardTextureBkg.loadFromFile("assets/cards/bkg-card.png");
    textures.add("cardTexture",cardTexture);
    textures.add("cardTextureBkg",cardTextureBkg);

    card = Card("pique","Roi",0,textures.get("cardTexture"),textures.get("cardTextureBkg"));
    //card.setScale(CARD_SCALE,CARD_SCALE);
    //card.setPosition(WIN_W/2 + 10,WIN_H*5/8);
	card.setPosition(100,100);

    bkg.loadFromFile("assets/bkg.jpg");
    textures.add("bkg",bkg);
    sf::Texture optionButtonTexture;
    optionButtonTexture.loadFromFile("assets/buttons/option.png");
    textures.add("option_button",optionButtonTexture);

    sf::Texture exitButtonTexture;
    exitButtonTexture.loadFromFile("assets/buttons/exit.png");
    textures.add("exit_button",exitButtonTexture);

    this->backgroundRectangle.setTexture(&textures.get("bkg"));

    m_nbPlayer = nbPlayer;

    optionButton.setScale(.8f,.8f);
    optionButton.setOrigin(optionButton.getBkg().getGlobalBounds().width/2,0);
    optionButton.setPosition(WIN_W*7/8,0);
    optionButton.setTexture(textures.get("option_button"));


    exitButton.setOrigin(exitButton.getBkg().getGlobalBounds().width/2,0);
    exitButton.setPosition(WIN_W - 80,0);
    exitButton.setScale(.8f,.8f);
    exitButton.setTexture(textures.get("exit_button"));

	information.setFont(font);
	
	displayInformation = false;

	std::ifstream ruleFile("assets/rules/" + rule + ".json",std::ios::in);
	if (ruleFile) {

		//std::string contenu;  // d�claration d'une cha�ne qui contiendra la ligne lue
		//std::getline(ruleFile, contenu);  // on met dans "contenu" la ligne
		
		Json::Value root;
		ruleFile >> root;
		
		std::string tag = root["texturePack"]["tag"].asString();
		if (boost::filesystem::is_directory("assets/texturePack")) {
			for (boost::filesystem::directory_iterator it("assets/texturePack"), end; it != end; ++it) {
				if (boost::filesystem::is_directory(*it))
				{
					
					std::ifstream configFile("assets/texturePack/" + it->path().filename().string() + "/config.json");
					if (configFile) {
						Json::Value configRoot;
						configFile >> configRoot;
						for (unsigned int i = 0; i < configRoot["tag"].size(); i++) {
							if (tag == configRoot["tag"][i].asString()) {
								texturePackPath = "assets/texturePack/" + it->path().filename().string() + "/";
								loadTextures(texturePackPath);
								break;
							}
							
						}if (texturePackPath == "") {
							DebugLog(SH_ERR, "Le pack de texture n a pas ete charge, peut etre un probleme de tag");
						}
						
					}
				}
			}
		}
		ruleFile.close();
		std::ifstream ruleFile("assets/rules/" + rule + ".json", std::ios::in);
		package = Package(Package::generatePackage(State::Verso, textures, ruleFile));
		ruleFile.close();
		package.setScale(CARD_SCALE, CARD_SCALE);
		
		package.setPosition(static_cast<float>((this->getWidth()*5/9) - 15), static_cast<float>((this->getHeight()/2) + package.getGlobalBounds().width));

		package.setRotation(270);
		package.shuffle();
	}

	cimetery.setScale(CARD_SCALE, CARD_SCALE);
	cimetery.setPosition(static_cast<float>((this->getWidth() * 4 / 9) +55), static_cast<float>((this->getHeight() / 2)));
	cimetery.setRotation(90);
	//cimetery.addCard(card);

	
	depart = card.getPosition();
	objectRotationStart = card.getRotation();

	//Generate Players
	DebugLog(SH_INFO, "il y a : " + std::to_string(nbPlayer) + " joueurs");
	players.push_back(std::make_shared<Player>());
	/*Py_SetPythonHome(L"C:\\Python34");
	py::scoped_interpreter guard{};*/
	
	for (unsigned int i = 1; i < nbPlayer; i++) {
		
		players.push_back(std::make_shared<Bot>());
		DebugLog(SH_INFO, "sfdgjikb");
		//Player &p = players[i];
		DebugLog(SH_INFO, "Bot swan generate");
	}
	
	partyState = "distribution";
	activePlayerID = 0;
	positions[0][0] = { WIN_W / 2 + 50,WIN_H - package.getGlobalBounds().width  - 50,0 };
	positions[0][1] = { WIN_W / 2 - package.getGlobalBounds().height, WIN_H - package.getGlobalBounds().width - 50,0 };
	positions[0][2] = { WIN_W / 2 + 50,WIN_H - package.getGlobalBounds().width *2 - 100,0 };
	positions[0][3] = { WIN_W / 2 - package.getGlobalBounds().height,WIN_H - package.getGlobalBounds().width *2 - 100 ,0 };

	positions[1][0] = { WIN_W / 2, 270, 180 };
	positions[1][1] = { WIN_W / 2 + 50 + package.getGlobalBounds().height, 270, 180 };
	positions[1][2] = { WIN_W / 2, 270 + package.getGlobalBounds().width + 50,180};
	positions[1][3] = { WIN_W / 2 + 50 + package.getGlobalBounds().height,270 + package.getGlobalBounds().width + 50,180};
	activeCard = std::make_shared<Card>(package.takeFirstCard());
	DebugLog(SH_INFO, package.getRotation());
}

void Party::draw(sf::RenderTarget& target,sf::RenderStates states) const{target.draw(Scene::backgroundRectangle);
    target.draw(optionButton);
    target.draw(exitButton);
    //target.draw(card);
	if (!package.isEmpty()) { target.draw(package); }
	if (!cimetery.isEmpty()) { target.draw(cimetery); }
	if (activeCard != NULL) { target.draw(*activeCard); /*DebugLog(SH_SPE, "je suis sens� afficher qq chose"); */}
	for (unsigned int i = 0; i < m_nbPlayer; i++) {
		for (unsigned int j = 0; j < nbCard ; j++) {
			if (players[i]->getCard(j) != NULL) {
				target.draw(*players[i]->getCard(j));
				//DebugLog(SH_SPE, players[i].getCard(j)->getPosition().y);
			}
		}
	}
	if (displayInformation) {
		target.draw(information);
	}
}

void Party::handle_std_animation() {
	//Distribution State
	if (partyState == "distribution") {
		//DebugLog(SH_SPE, cardID);
		//unsigned int npoints = 100;
		unsigned int npoints = 1;
		float time = 0.f;
		if (alp <= npoints) {
			Party::interpolation(*activeCard, package.getPosition(), sf::Vector2f(positions[activePlayerID][cardID][0], positions[activePlayerID][cardID][1]),npoints,time,alp);
			Party::objectRotation(*activeCard, package.getRotation(), positions[activePlayerID][cardID][2], false, npoints, time, alp);
			alp++;
		}
		if (alp > npoints) {
			players[activePlayerID]->switchCard(activeCard, Player::nbToPos(cardID).x, Player::nbToPos(cardID).y);
			if (players[1]->getCard(0) == NULL) {
				DebugLog(SH_SPE,"oui");
			}
			if (activePlayerID == m_nbPlayer - 1) {
				activePlayerID = 0;
				cardID++;
			}
			else {
				activePlayerID++;
			}
			alp = 0;
			activeCard = std::make_shared<Card>(package.takeFirstCard());
		}
		if (cardID == nbCard) {
			activeCard.reset();
			partyState = "cardReturned";
			DebugLog(SH_INFO, "distribution terminee");
			players[0]->getCard(2)->returnCard();
			players[0]->getCard(3)->returnCard();
			setInformation("Press \"p\" to start the game ", sf::Color::White);
			
		}
	}

	if (partyState == "cardChanging") {
		//DebugLog(SH_INFO, "Le joueur 1 commence a jouer");
		//activeCard->prensentation();
		unsigned int npoints = 100;
		float time = 0.5f;
		if (alp <= npoints) {
			Party::interpolation(*activeCard, package.getPosition(), players[activePlayerID]->getCard(cardChangedID)->getPosition(), npoints, time, alp);
			Party::objectRotation(*activeCard, package.getRotation(), players[activePlayerID]->getCard(cardChangedID)->getRotation(), false, npoints, time, alp);
			alp++;
		}
		else if (alp > npoints) {

			partyState = "cardChanging2";
			players[activePlayerID]->switchCard(activeCard, cardChangedID);
			alp = 0;
			players[activePlayerID]->getCard(cardChangedID)->returnCard();
			activeCard->returnCard();
			DebugLog(SH_SPE, positions[activePlayerID][cardChangedID][2]);
			//if(activePlayerID == m_nbPlayer - 1){ activePlayerID = 0; }else{ activePlayerID++; }
			
		}
	}

	if (partyState == "cardChangingCimetery") {
		unsigned int npoints = 100;
		float time = 0.5f;
		if (alp <= npoints) {
			Party::interpolation(*activeCard, cimetery.getPosition(), players[activePlayerID]->getCard(cardChangedID)->getPosition(), npoints, time, alp);
			Party::objectRotation(*activeCard, cimetery.getRotation(), players[activePlayerID]->getCard(cardChangedID)->getRotation(), false, npoints, time, alp);
			alp++;
		}
		else if (alp > npoints) {
			partyState = "cardChanging2";
			players[activePlayerID]->switchCard(activeCard, cardChangedID);
			alp = 0;

			players[activePlayerID]->getCard(cardChangedID)->returnCard();
			activeCard->returnCard();
		}
	}

	else if (partyState == "cardChanging2") {
		unsigned int npoints = 100;
		float time = 0.5f;
		if (alp <= npoints) {
			Party::interpolation(*activeCard, sf::Vector2f(positions[activePlayerID][cardChangedID][0], positions[activePlayerID ][cardChangedID][1]), cimetery.getPosition(), npoints, time, alp);
			Party::objectRotation(*activeCard, positions[activePlayerID][cardChangedID][2], cimetery.getRotation(), true, npoints, time, alp);
			alp++;
		}
		else if (alp > npoints) {
			alp = 0;
			partyState = "party";
			cimetery.addCard(*activeCard);
			activeCard = NULL;
			if (activePlayerID == m_nbPlayer - 1) { activePlayerID = 0; }else { activePlayerID++; }
			DebugLog(SH_INFO, "C'est au tour du joueur " + std::to_string(activePlayerID) + " de jouer");
			//players[0].switchCard(activeCard, cardChangedID);
		}
	}

	if (partyState == "party") {
		if (activePlayerID > 0) {
			//py::scoped_interpreter guard{};
			//DebugLog(SH_INFO, "C'est au tour du joueur " + std::to_string(activePlayerID) + " de jouer");
			//DebugLog(SH_SPE, std::get<0>(players[activePlayerID]->choosePackage()));
			//DebugLog(SH_SPE, std::to_string(cimetery.getFirstCard().getColor()));
			std::string packageChoosen = players[activePlayerID]->choosePackage(cimetery.getFirstCard().getNumber().c_str(), cimetery.getFirstCard().getColor().c_str(), cimetery.getFirstCard().getValue());

			//std::tuple<std::string,int,int> changeInformation = players[activePlayerID]->choosePackage(); //Tuple de 2 donn�es, 
			/*std::get<0> -> String : Packet choisi, "package" pour la pioche et "cimetery" pour le cimetiere
			  std::get<1> ->int : ID de la carte a �changer*/
			
			
			cardChangedID = players[activePlayerID]->chooseCard();

			if (packageChoosen == "package") {
				partyState = "cardChanging";
				activeCard = std::make_shared<Card>(package.takeFirstCard());
				activeCard->returnCard();
				activeCard->setPosition(package.getPosition());
			}
			else if (packageChoosen == "cimetery") {
				activeCard = std::make_shared<Card>(cimetery.takeFirstCard());
				activeCard->setPosition(cimetery.getPosition());
				partyState = "cardChangingCimetery";
			}
			else {
				DebugLog(SH_ERR, "Erreur du bot, la fonction ne retourne pas un packet valide");
			}


			/*cardChangedID = std::get<1>(players[activePlayerID]->choosePackage());
			if (std::get<0>(players[activePlayerID]->choosePackage()) == "package") {
				partyState = "cardChanging";
				activeCard = std::make_shared<Card>(package.takeFirstCard());
				activeCard->returnCard();
				activeCard->setPosition(package.getPosition());
			}
			else if (std::get<0>(players[activePlayerID]->choosePackage()) == "cimetery") {
				activeCard = std::make_shared<Card>(cimetery.takeFirstCard());
				activeCard->setPosition(cimetery.getPosition());
				partyState = "cardChangingCimetery";
			}*/
			//activePlayerID = 0;
		}
	}
	
}

void Party::run(){}

void Party::handle_std_event(sf::Event event) {
	switch (event.type) {
	case sf::Event::MouseButtonPressed:
		if (event.mouseButton.button == sf::Mouse::Left) {
			if (exitButton.hasClick(event)) {
				buttonClickAction = "exit";
			}
			else if (exitButton.hasClick(event)) {
				buttonClickAction = "option";
			}
			if (activePlayerID == 0) {
				if (partyState == "party") {
					if (package.hasClick(event)) {
						activeCard = std::make_shared<Card>(package.takeFirstCard());
						activeCard->returnCard();
						activeCard->setPosition(package.getPosition());
						partyState = "cardChange";
						//activeCard->setState(State::Recto);
						//DebugLog(SH_SPE, activeCard->getGlobalBounds().height);
					}
					else if (cimetery.hasClick(event)) {
						activeCard = std::make_shared<Card>(cimetery.takeFirstCard());
						activeCard->setPosition(cimetery.getPosition());
						//activeCard->setOrigin(0, activeCard->getGlobalBounds().height);
						partyState = "cardChangeCimetery";
					}
				}

				else if (partyState == "cardChange" || partyState == "cardChangeCimetery") {

					for (unsigned int i = 0; i < 4; i++) {
						if (players[0]->getCard(i)->hasClick(event)) {
							cardChangedID = i;
							if (partyState == "cardChange") { partyState = "cardChanging"; }
							else { partyState = "cardChangingCimetery"; }
							DebugLog(SH_SPE, cardChangedID);
							break;
						}
					}
				}
			}

			

		}
	case sf::Event::KeyPressed:
		if (event.key.code == sf::Keyboard::P) {
			if (partyState == "cardReturned") {
				players[0]->getCard(2)->returnCard();
				players[0]->getCard(3)->returnCard();
				partyState = "party";
				displayInformation = false;
				activePlayerID = 0;
			}
			else if (partyState == "party") {
				if (activePlayerID == 0) {
					
				}
			}
		}

	default:
		;
	}



}

void Party::setInformation(std::string text,sf::Color color) {
	information.setString(text);
	information.setFillColor(color);
	information.setOrigin(information.getGlobalBounds().width / 2, information.getGlobalBounds().height / 2);
	information.setPosition(this->getWidth() / 2, this->getHeight() / 2);
	displayInformation = true;
}

void Party::loadTextures(std::string path) {
	if (boost::filesystem::is_directory(path)) {
		sf::Texture texture;
		for (boost::filesystem::directory_iterator it(path), end; it != end; ++it){
			if (boost::filesystem::is_regular_file(it->status())) {
				if (func::split(it->path().filename().string(), '.')[1] == "png" || func::split(it->path().filename().string(), '.')[1] == "jpg") {
					texture.loadFromFile(path + it->path().filename().string());
					textures.add(func::split(it->path().filename().string(), '.')[0], texture);
				}
			}
		}
	}

}


float Party::lerp2d(sf::Vector2f start, sf::Vector2f end, float offset, unsigned int i) {
	if (end.x - start.x > 0) { // Si l'objet se d�place sur la droite
		return start.y + (offset * i) * (end.y - start.y) / (end.x - start.x);
	}
	else {
		return start.y + (-offset * i) * (end.y - start.y) / (end.x - start.x);
	}
	/*
	Formule d'interpolation lin�aire, avec c = start.x + offset * i
	*/

}


void Party::interpolation(sf::Transformable& object, sf::Vector2f start, sf::Vector2f end, unsigned int npoints, float time, unsigned int i) {
	/*
	object : Object a bouger
	start : Position de d�part de l'objet
	end : position finale de l'objet
	npoints : nombre de d�placemment
	time : en cb de temps (en secondes)
	i : suivi de l'avancement de l'objet (en nombre de d�placement)
	*/

	float dt = time / npoints;
	float offset = std::abs(end.x - start.x) / npoints;
	boost::this_thread::sleep(boost::posix_time::milliseconds(static_cast<unsigned int>(dt * 1000)));


	if (end.x - start.x > 0) { // Si l'objet se d�place sur la droite
		object.setPosition(start.x + i * offset, lerp2d(start, end, offset, i));
	}
	else {
		object.setPosition(start.x - i * offset, lerp2d(start, end, offset, i));
	}
	//DebugLog(SH_SPE, "x : " + std::to_string(object.getPosition().x));
	//DebugLog(SH_SPE, "y : " + std::to_string(object.getPosition().y));

}

void Party::objectRotation(sf::Transformable& object, float start, float end, bool left, unsigned int npoints, float time, unsigned int i) {

	float dt = time / npoints;
	float offset = std::abs(end - start) / npoints;
	boost::this_thread::sleep(boost::posix_time::milliseconds(static_cast<unsigned int>(dt * 1000)));
	if (i > 0) {
		/*if (left) {
			object.rotate(offset);
		}
		else {
			object.rotate(-offset);

		}*/
		if (end - start > 0) {
			object.rotate(offset);
		}
		else {
			object.rotate(-offset);
		}
	}

}