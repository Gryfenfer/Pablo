#ifndef PARTYCONFIGURATOR_HPP_INCLUDED
#define PARTYCONFIGURATOR_HPP_INCLUDED

#include <fstream>
#include <iostream>

#include <boost/filesystem.hpp>


#include "../abstract/scene.hpp"
#include "../abstract/container.hpp"
#include "../constants.hpp"
#include "../abstract/button.hpp"

#include "../abstract/functions.hpp"

//#include <filesystem>

//namespace fs = std::filesystem;

class PartyConfigurator : public Scene{

private:
    int nbPlayer;
    std::string rule;
	unsigned int ruleNum;
    unsigned int difficulty;
    sf::CircleShape shape;

    int minNbPlayer;
    int maxNbPlayer;

    sf::Font font;
    sf::Text title;
    sf::Text nbPlayerLabel;
    sf::Text playerLabel;
    sf::Text difficultyLabel;
    sf::Text difficultyText;
    sf::Text playButton;
	sf::Text rulesText;

	std::string rulesPath;

    Container<sf::Texture> textures;

    std::vector<std::string> rulesList;

    //Sprites
    //sf::Sprite leftArrowNbPlayer;
    //sf::Sprite rightArrowNbPlayer;

    //sf::Sprite leftArrowDifficulty;
    //sf::Sprite rightArrowDifficulty;

    Button rightArrowNbPlayer;
    Button leftArrowNbPlayer;
    Button leftArrowDifficulty;
    Button rightArrowDifficulty;
	Button leftArrowRules;
	Button rightArrowRules;

    sf::Text exitText;


public:
    PartyConfigurator();
    ~PartyConfigurator();

    virtual void draw(sf::RenderTarget& target,sf::RenderStates states) const;
    virtual void handle_std_animation();
    virtual void handle_std_event(sf::Event event);



    std::string difficultyToString(unsigned int d){
        switch(d){
            case 0 : return "easy";
            case 1 : return "medium";
            case 2 : return "hard";
			default: return "";
        }
    }


};

#endif // PARTYCONFIGURATOR_HPP_INCLUDED
