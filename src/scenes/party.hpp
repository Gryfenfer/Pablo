#ifndef PARTY_HPP_INCLUDED
#define PARTY_HPP_INCLUDED

#include <boost/filesystem.hpp>
#include <boost/thread/thread.hpp>

#include "../abstract/scene.hpp"
#include "../abstract/container.hpp"
#include "../abstract/button.hpp"
#include "../objects/card.hpp"
#include "../objects/package.hpp"
#include "../abstract/functions.hpp"
#include "../json/json.h"
#include "../player/player.hpp"
#include "../player/bot.hpp"
#include "../abstract/functions.hpp"
#include <pybind11/embed.h>
#include <pybind11/pybind11.h>
#include <iostream>
#include <tuple>
#include "lua.hpp"
namespace py = pybind11;

class Party : public Scene{
private:
    unsigned int m_nbPlayer;

    Container<sf::Texture> textures;

    sf::Texture bkg;

    Button optionButton;
    Button exitButton;

    Card card;
	Package cimetery;
	Package package;
	std::string texturePackPath = "";
	
	void loadTextures(std::string path);

	static float lerp(float start,float end, float t);
	static float lerp2d(sf::Vector2f start, sf::Vector2f end, float offset, unsigned int i);
	static void interpolation(sf::Transformable& object, sf::Vector2f start, sf::Vector2f end,unsigned int npoints,float time,unsigned int i);
	static void objectRotation(sf::Transformable& object, float start, float end, bool left, unsigned int npoints, float time, unsigned int i);
	unsigned int alp = 0;
	sf::Vector2f depart;
	float objectRotationStart;

	std::string partyState;
	unsigned int nbCard = 4;
	unsigned int activePlayerID;
	unsigned int cardID = 0;

	sf::Font font;

	std::array<std::array<std::array<float,3>,4>,4> positions;
	std::vector<std::shared_ptr<Player>> players;
	std::shared_ptr<Card> activeCard = NULL;
	unsigned int cardChangedID;
	


	sf::Text information;
	void setInformation(std::string text,sf::Color color);
	//Triggers
	bool displayInformation;




public:
    Party(unsigned int nbPlayer,std::string rule);

	void run();

    //Virtual function
    virtual void draw(sf::RenderTarget& target,sf::RenderStates states) const;
    virtual void handle_std_animation();
    virtual void handle_std_event(sf::Event event);


};


#endif // PARTY_HPP_INCLUDED
