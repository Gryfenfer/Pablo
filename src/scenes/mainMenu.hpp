#ifndef MENU_HPP_INCLUDED
#define MENU_HPP_INCLUDED

#include <iostream>

#include "../abstract/scene.hpp"


class MainMenu : public Scene{

private:
    sf::Font font;
    sf::Text playButton;
    sf::Text optionButton;
    sf::Text exitButton;



public:
    //Constructor
    MainMenu();

    //Virtual function
    virtual void draw(sf::RenderTarget& target,sf::RenderStates states) const;
    virtual void handle_std_animation();
    virtual void handle_std_event(sf::Event event);

};

#endif // MENU_HPP_INCLUDED
