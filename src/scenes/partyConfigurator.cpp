#include "partyConfigurator.hpp"
#include "../constants.hpp"



PartyConfigurator::PartyConfigurator() : Scene(WIN_W,WIN_H,sf::Vector2f(0.f,0.f),sf::Color::Black,0.6){
    //Load the rulesList
    //for(auto& p: std::filesystem::directory_iterator("assets/rules")){
      //  std::cout << p << '\n';}

	rulesPath = "assets/rules/";

    nbPlayer = 2;
    rule = "default";
    difficulty = 0;

    minNbPlayer = 2;
    maxNbPlayer = 4;

    shape = sf::CircleShape(50);
    shape.setFillColor(sf::Color(255, 0, 0, Scene::m_opacity));

    //Labels
    if (!font.loadFromFile("assets/fonts/arial.ttf"))
    {
        std::cout << "error" << std::endl;
    }
    //Title
    title.setFont(font);
    title.setString("Configure your party");
    title.setFillColor(sf::Color::White);
    title.setOrigin(title.getLocalBounds().width/2,0);
    title.setPosition(WIN_W/2,5);

    //"Nombre de Joueurs"
    playerLabel.setFont(font);
    playerLabel.setString("Nombre de Joueurs");
    playerLabel.setFillColor(sf::Color::White);
    playerLabel.setCharacterSize(24);
    playerLabel.setOrigin(playerLabel.getLocalBounds().width/2,0);
    playerLabel.setPosition(WIN_W/2,(WIN_H/5) - 40);

    //Print the number of players in the party
    nbPlayerLabel.setFont(font);
    nbPlayerLabel.setString(std::to_string(nbPlayer));
    nbPlayerLabel.setFillColor(sf::Color::Red);
    nbPlayerLabel.setCharacterSize(75);
    nbPlayerLabel.setOrigin(nbPlayerLabel.getLocalBounds().width/2,0);
    nbPlayerLabel.setPosition(WIN_W/2,(WIN_H/5) + 10);

	rulesText.setFont(font);
	rulesText.setString(rule);
	rulesText.setFillColor(sf::Color::White);
	rulesText.setCharacterSize(75);
	rulesText.setOrigin(rulesText.getLocalBounds().width / 2, 0);
	rulesText.setPosition(WIN_W / 2, (WIN_H*3/ 5) + 10);

    difficultyLabel.setFont(font);
    difficultyLabel.setString("Difficulté");
    difficultyLabel.setFillColor(sf::Color::White);
    difficultyLabel.setCharacterSize(24);
    difficultyLabel.setOrigin(difficultyLabel.getLocalBounds().width/2,0);
    difficultyLabel.setPosition(WIN_W/2,(WIN_H*2/5) - 15);

    difficultyText.setFont(font);
    difficultyText.setString(difficultyToString(difficulty));
    difficultyText.setFillColor(sf::Color::Red);
    difficultyText.setCharacterSize(60);
    difficultyText.setOrigin(difficultyText.getLocalBounds().width/2,0);
    difficultyText.setPosition(WIN_W/2,(WIN_H*2/5) + 10);

	


    playButton.setFont(font);
    playButton.setString("play");
    playButton.setFillColor(sf::Color::White);
    playButton.setOrigin(playButton.getLocalBounds().width/2,0);
    playButton.setPosition(WIN_W/2,(WIN_H*4/5) + 30);

    exitText.setFont(font);
    exitText.setString("x");
    exitText.setFillColor(sf::Color::White);
    exitText.setPosition(10,10);

    //Textures
    sf::Texture right_arrow;
    right_arrow.loadFromFile("arrow-right.png");
    this->textures.add("right_arrow",right_arrow);

    sf::Texture left_arrow;
    left_arrow.loadFromFile("arrow-left.png");
    this->textures.add("left_arrow", left_arrow);

	this->rightArrowNbPlayer.setTexture(textures.get("right_arrow"));
	this->rightArrowNbPlayer.setScale(0.25f, 0.25f);
    this->rightArrowNbPlayer.setPosition(WIN_W/2 + (WIN_W/3)/2 + 10 + rightArrowNbPlayer.getBkg().getLocalBounds().width * rightArrowNbPlayer.getScale().x,(WIN_H/5) + 10);
	DebugLog(SH_SPE, std::to_string(rightArrowNbPlayer.getBkg().getGlobalBounds().width));
   
    

    if(nbPlayer==maxNbPlayer){rightArrowNbPlayer.setEnable();}


    this->leftArrowNbPlayer.setPosition(WIN_W/2 - (WIN_W/3) - 10,(WIN_H/5) + 10);
    this->leftArrowNbPlayer.setScale(0.25f,0.25f);
    this->leftArrowNbPlayer.setTexture(textures.get("left_arrow"));


    if(nbPlayer==minNbPlayer){leftArrowNbPlayer.setEnable();}


    this->leftArrowDifficulty.setPosition(WIN_W/2 - (WIN_W/3) - 10,(WIN_H*2/5) + 20);
    this->leftArrowDifficulty.setScale(0.25,0.25);
    this->leftArrowDifficulty.setTexture(textures.get("left_arrow"));
    if(difficulty == 0){leftArrowDifficulty.setEnable();}


	this->rightArrowDifficulty.setTexture(textures.get("right_arrow"));
	this->rightArrowDifficulty.setScale(0.25, 0.25);
    this->rightArrowDifficulty.setPosition(WIN_W/2 + (WIN_W/3)/2 + 10 + rightArrowDifficulty.getBkg().getLocalBounds().width * rightArrowDifficulty.getScale().x,(WIN_H*2/5) + 20);
    
    


    if(difficulty == 3){rightArrowDifficulty.setEnable();}

	this->leftArrowRules.setTexture(textures.get("left_arrow"));
	this->leftArrowRules.setPosition(WIN_W / 2 - (WIN_W / 3) - 10, (WIN_H * 3 / 5) + 25);
	this->leftArrowRules.setScale(0.25, 0.25);
	

	this->rightArrowRules.setTexture(textures.get("right_arrow"));
	this->rightArrowRules.setScale(0.25, 0.25);
	this->rightArrowRules.setPosition(WIN_W / 2 + (WIN_W / 3) / 2 + 10 + rightArrowRules.getBkg().getLocalBounds().width * rightArrowRules.getScale().x, (WIN_H * 3 / 5) + 25);
	

	//List rules

	if (boost::filesystem::is_directory(rulesPath)) {
		for (boost::filesystem::directory_iterator it(rulesPath), end; it != end; ++it) {
			if (boost::filesystem::is_regular_file(it->status())) {
				if (func::split(it->path().filename().string(), '.')[1] == "json"){
					std::cout << it->path().filename() << std::endl;
					rulesList.push_back(func::split(it->path().filename().string(),'.')[0]);
					DebugLog(SH_INFO, rulesList[rulesList.size() - 1]);
					if (rulesList[rulesList.size() - 1] == "default") {
						ruleNum = rulesList.size() - 1;
					}
				}
			}
		}
	}

}

PartyConfigurator::~PartyConfigurator(){
}

void PartyConfigurator::draw(sf::RenderTarget& target,sf::RenderStates states) const{
    target.draw(Scene::backgroundRectangle); //Drawing the background
    //target.draw(shape);
    //target.draw(rightArrowNbPlayer);
    target.draw(title);
    target.draw(nbPlayerLabel);
    target.draw(playerLabel);
    target.draw(leftArrowDifficulty);
    target.draw(rightArrowDifficulty);
    target.draw(difficultyLabel);
    target.draw(difficultyText);
    target.draw(rightArrowNbPlayer);
    target.draw(leftArrowNbPlayer);
    target.draw(playButton);
    target.draw(exitText);
	target.draw(rulesText);
	target.draw(rightArrowRules);
	target.draw(leftArrowRules);
}
void PartyConfigurator::handle_std_animation(){}
void PartyConfigurator::handle_std_event(sf::Event event){
    switch(event.type){
        case sf::Event::MouseButtonPressed:
            if (event.mouseButton.button == sf::Mouse::Left){
                if (leftArrowNbPlayer.hasClick(event) && leftArrowNbPlayer.getEnable()){
                    DebugLog(SH_INFO, "the number of player grow down");
                    nbPlayer--;
                    nbPlayerLabel.setString(std::to_string(nbPlayer));
                    if(nbPlayer == minNbPlayer){
                        //rightArrowNbPlayer.setColor(sf::Color(255,255,0,128));
                        leftArrowNbPlayer.setEnable();
                    }
                    else if(nbPlayer == maxNbPlayer - 1){
                        rightArrowNbPlayer.setEnable();
                    }
                }

                else if (rightArrowNbPlayer.hasClick(event) && rightArrowNbPlayer.getEnable()){
                    DebugLog(SH_INFO, "the number of player grow up");
                    nbPlayer++;
                    nbPlayerLabel.setString(std::to_string(nbPlayer));
                    if(nbPlayer == maxNbPlayer){
                        //rightArrowNbPlayer.setColor(sf::Color(255,255,0,128));
                        rightArrowNbPlayer.setEnable();
                    }
                    else if(nbPlayer == minNbPlayer + 1){
                        leftArrowNbPlayer.setEnable();
                    }
                }

                else if(rightArrowDifficulty.hasClick(event) && rightArrowDifficulty.getEnable()){
                    DebugLog(SH_INFO, "the difficulty grow up");
                    difficulty++;
                    difficultyText.setString(difficultyToString(difficulty));
                    difficultyText.setOrigin(difficultyText.getLocalBounds().width/2,0);
                    difficultyText.setPosition(WIN_W/2,(WIN_H*2/5) + 10);
                    if (difficulty == 2){
                        rightArrowDifficulty.setEnable();
                    }else if (difficulty == 1){
                        leftArrowDifficulty.setEnable();
                    }
                }

                else if(leftArrowDifficulty.hasClick(event) && leftArrowDifficulty.getEnable()){
                    DebugLog(SH_INFO, "the difficulty grow down");
                    difficulty--;
                    difficultyText.setString(difficultyToString(difficulty));
                    difficultyText.setOrigin(difficultyText.getLocalBounds().width/2,0);
                    difficultyText.setPosition(WIN_W/2,(WIN_H*2/5) + 10);
                    if (difficulty == 0){
                        leftArrowDifficulty.setEnable();
                    }else if (difficulty == 1){
                        rightArrowDifficulty.setEnable();
                    }
                }

				else if (leftArrowRules.hasClick(event) && leftArrowRules.getEnable()) {
					DebugLog(SH_INFO, "the rules grow down");
					if (ruleNum == 0) {
						ruleNum = rulesList.size() - 1;
					}
					else {
						ruleNum--;
					}
					rulesText.setString(rulesList[ruleNum]);
					rulesText.setOrigin(rulesText.getLocalBounds().width / 2, 0);
					rulesText.setPosition(WIN_W / 2, (WIN_H * 3 / 5) + 10);
					
				}

				else if (rightArrowRules.hasClick(event) && rightArrowRules.getEnable()) {
					DebugLog(SH_INFO, "the rules grow up");
					if (ruleNum == rulesList.size() - 1) {
						ruleNum = 0;
					}
					else {
						ruleNum++;
					}
					rulesText.setString(rulesList[ruleNum]);
					rulesText.setOrigin(rulesText.getLocalBounds().width / 2, 0);
					rulesText.setPosition(WIN_W / 2, (WIN_H * 3 / 5) + 10);

				}


                else if(playButton.getGlobalBounds().contains(event.mouseButton.x,event.mouseButton.y)){
                    DebugLog(SH_INFO,"The game will start");
                    buttonClickAction = "start+" + std::to_string(nbPlayer) + "+" + std::to_string(difficulty) + "+" + rulesList[ruleNum];

                }

                else if(exitText.getGlobalBounds().contains(event.mouseButton.x,event.mouseButton.y)){
                    buttonClickAction = "exit";

                }

            }
        default:
            ;
    }


}

