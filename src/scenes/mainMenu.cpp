#include "mainMenu.hpp"
#include "../constants.hpp"
extern "C" {
//#include "../player/call_function.h" 
}

MainMenu::MainMenu() : Scene(WIN_W,WIN_H,sf::Vector2f(0.f,0.f),sf::Color(9,106,9),1){
    //Shape for test
    //shape = sf::CircleShape(400.f);
    //shape.setFillColor(sf::Color::Blue);

    buttonClickAction = "";

    if (!font.loadFromFile("assets/fonts/arial.ttf"))
    {
        std::cout << "error" << std::endl;
    }

    //Play button
    playButton.setFont(font);
    playButton.setString("Play");
    playButton.setFillColor(sf::Color::Blue);
    playButton.setPosition(WIN_W/2 + 60,WIN_H/2 + 20);

    //Option Button
    optionButton.setFont(font);
    optionButton.setString("Options");
    optionButton.setFillColor(sf::Color::Blue);
    optionButton.setPosition(WIN_W/2 + 60,WIN_H/2 + 64);
	//callF();
	

    //Exit Button
    exitButton.setFont(font);
    exitButton.setString("Exit");
    exitButton.setFillColor(sf::Color::Blue);
    exitButton.setPosition(WIN_W/2 + 60,WIN_H/2 + 108);
}

void MainMenu::draw(sf::RenderTarget& target,sf::RenderStates states) const{
    target.draw(Scene::backgroundRectangle); //Drawing the background

    target.draw(playButton); //Drawing a shape for testing
    target.draw(optionButton);
    target.draw(exitButton);
}

void MainMenu::handle_std_animation(){

}
void MainMenu::handle_std_event(sf::Event event){
    switch(event.type){
        case sf::Event::MouseMoved:

            //Play button hover
            if(playButton.getGlobalBounds().contains(event.mouseMove.x,event.mouseMove.y)){
                playButton.setFillColor(sf::Color::Red);
            }else{playButton.setFillColor(sf::Color::Blue);}

            //Play button hover
            if(optionButton.getGlobalBounds().contains(event.mouseMove.x,event.mouseMove.y)){
                optionButton.setFillColor(sf::Color::Red);
            }else{optionButton.setFillColor(sf::Color::Blue);}

            //Play button hover
            if(exitButton.getGlobalBounds().contains(event.mouseMove.x,event.mouseMove.y)){
                exitButton.setFillColor(sf::Color::Red);
            }else{exitButton.setFillColor(sf::Color::Blue);}

        case sf::Event::MouseButtonPressed:
            if(event.mouseButton.button == sf::Mouse::Left){
                if(playButton.getGlobalBounds().contains(event.mouseButton.x,event.mouseButton.y)){
                    buttonClickAction = "Play";
                    playButton.setFillColor(sf::Color::Blue);
                }
                if(optionButton.getGlobalBounds().contains(event.mouseButton.x,event.mouseButton.y)){
                    buttonClickAction = "Options";
                    optionButton.setFillColor(sf::Color::Blue);
                }
                if(exitButton.getGlobalBounds().contains(event.mouseButton.x,event.mouseButton.y)){
                    buttonClickAction = "Exit";
                    exitButton.setFillColor(sf::Color::Blue);
                }
            }
        default:
            break;
    }
}


