#include "game.hpp"

Game::Game() :
    //window(sf::VideoMode(WIN_W, WIN_H), WIN_TITLE,sf::Style::Titlebar | sf::Style::Close)
    //window(sf::VideoMode(WIN_W, WIN_H), WIN_TITLE,sf::Style::None)
    #ifdef FULLSCREEN_MODE
        window(sf::VideoMode(),WIN_TITLE,sf::Style::Fullscreen)
    #else
        window(sf::VideoMode(WIN_W, WIN_H), WIN_TITLE,sf::Style::None)
    #endif
    {
    //activeScene = std::make_unique<MainMenu>(mainMenu);
    mainMenu = std::make_shared<MainMenu>();


    activeScene = mainMenu;

    drawMainMenu = true;
    drawPartyConf = false;
    drawParty = false;

    window.setView(mainMenu->getMainView());


}

void Game::run(){
	Py_SetPythonHome(L"C:\\Python34");
	py::scoped_interpreter guard{};
    while (this->window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            switch(event.type){
                case sf::Event::Closed:
                    this->window.close();
                    break;

                default:
                    break;
            }

            handle_event(event);
            activeScene->handle_std_event(event);
            interpret();
        }

        activeScene->handle_std_animation();


        window.clear();
        this->render();
        window.display();
    }
}

void Game::render(){
    if(drawMainMenu){window.draw(*mainMenu);}
    if(drawPartyConf){window.draw(*partyConf);}
    if(drawParty){window.draw(*party);}
}

void Game::interpret(){

    if (activeScene == mainMenu){
            std::string action = mainMenu->getButtonClickAction();
            if (action != ""){
                if (action == "Play"){
                    //Start the scene with choices for the game
                    partyConf = std::make_shared<PartyConfigurator>(); //initialisation of the Scene
                    window.setView(partyConf->getMainView());
                    activeScene = partyConf; //Switch the activeScene;
                    drawPartyConf = true;
                    mainMenu->resetButtonClickAction();

                }
                else if (action == "Options"){
                    //Start the option scene
                    mainMenu->resetButtonClickAction();

                }
                else if (action == "Exit"){
                    this->window.close(); //Close the window
                }
                //mainMenu->getButtonClickAction()
            }
    }

    else if (activeScene == partyConf){
        std::string action = partyConf->getButtonClickAction();
        if (func::split(action,'+')[0] == "start"){
            party = std::make_shared<Party>(std::stoi(func::split(action, '+')[1]), func::split(action, '+')[3]);
            window.setView(party->getMainView());
            activeScene = party;
            mainMenu = 0;
            partyConf = 0;

            drawParty = true;
            drawPartyConf = false;
            drawMainMenu = false;

        }
        else if (action == "exit"){
            window.setView(mainMenu->getMainView());
            activeScene = mainMenu; //Switch the activeScene;
            drawPartyConf = false;

        }
    }
    else if (activeScene == party){
        std::string action = party->getButtonClickAction();
        if (action == "exit"){
            window.close();

        }
    }
}


void Game::handle_event(sf::Event event){
    if (event.type == sf::Event::KeyPressed)
    {
        if (event.key.code == sf::Keyboard::T)
        {
            openShell();
        }
    }
}
